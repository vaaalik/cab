from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
import user_management
import settings
import file_storage.views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'Cabinet.views.index', name='home'),
    # url(r'^Cabinet/', include('Cabinet.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
   url(r'^admin/', include(admin.site.urls)),
   url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
   url(r'^register/(?P<flag>[a-z]{0,7})/$', 'user_management.views.sign_up', name='Sign Up'),
   url(r'^reg/$','user_management.views.reg'),
   url(r'^subjs/$', 'file_storage.views.subjects'),
   url(r'^profile/$', login_required(fuser_management.views.edit_profile)),
   url(r'^(?P<username>\d{7})/lab/(?P<lab_id>\d{1,3})/$', 'file_storage.views.lab_view', name='lab_view'),
  url(r'^resset_passw/$', 'user_management.views.resset_passw'),
  #url(r'^resset_passw/$', 'Cabinet.views.test'),
  url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
  url(r'^post_comment/$', 'file_storage.views.post_comment'),
  url(r'^howto/$','user_management.views.howto'),
  url(r'^staff/lab/(?P<lab_id>\d{1,3})/$', 'file_storage.views.staff_lab')

)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
