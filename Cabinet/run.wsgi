import os, sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))
os.environ['DJANGO_SETTINGS_MODULE'] = 'Cabinet.settings'
os.environ['PYTHON_EGG_CACHE'] = '/var/www/Cabinet/.python-eggs'
 
from django.core.handlers.wsgi import WSGIHandler
application = WSGIHandler()
