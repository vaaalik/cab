# -*- coding: UTF-8 -*-

from django.template.response import TemplateResponse
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.utils import simplejson
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from user_management.models import UserProfile
import random
import string
import sys



def index(request):
    if request.method == "POST":
        data = request.POST
        usern = data.get('username')
        passw = data.get('password')
        user = authenticate(username=usern, password=passw)
        if user:
            login(request, user)
            return redirect('/subjs/')
        else:
            return TemplateResponse(request, 'index.html', {'error': True, 'name': usern})
    elif request.method == "GET":
        if request.user.is_authenticated():
            return redirect('/subjs/')
        else:
            return TemplateResponse(request, 'index.html')

def reg(request):
    return TemplateResponse(request, 'sign_up_all.html')

def sign_up(request, flag):
    if request.method == "POST":
        data = request.POST
        usr = User()
        usr.username = data.get('username')
        usr.set_password(data.get('password1'))
        usr.email = data.get('email')
        usr.first_name = data.get('name')
        usr.last_name = data.get('last_name')
        if flag =='teacher':
            usr.is_staff=True
            usr.is_superuser=True
        usr.save()
        profile = UserProfile(user=usr)
        grp = data.get('group')
        profile.group_num = grp
        if flag == "student":
            profile.course = int(grp[0])
        profile.save()
        return HttpResponseRedirect('/')
    elif request.method == "GET":
        if flag == None:
            return TemplateResponse(request, 'sign_up_all.html')
        elif flag == 'student':
            return TemplateResponse(request, 'sign_up_student.html')
        elif flag == 'teacher':
            return TemplateResponse(request, 'sign_up_teacher.html')



def resset_passw(request):
    if request.method == 'POST':
        data = request.POST
        print >> sys.stderr, data
        try:
            usr = User.objects.get(email=data.get('email'))
            new_pasw = gen_pasw
            usr.set_password(new_pasw)
            send_mail(u"Відновлення пароля", u"Ваш новий пароль %s" % new_pasw, 'zzapadloo@gmail.com', (usr.email,))
            return TemplateResponse(request, 'index.html', {"succes":1})
        except User.DoesNotExist:
            return HttpResponse(simplejson({'success': 0}))


def howto(request):
    if request.method=='GET':
        return TemplateResponse(request, 'howto.html')




def gen_pasw():
    return ''.join(random.choice(string.ascii_letters + string.digits) for x in range(8))
