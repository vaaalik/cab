# -*- coding: UTF-8 -*-
from django.db import models
from django import forms
from user_management.models import UserProfile
import sys
import os



class Subject(models.Model):
    '''Model for subject by courses'''
    course = models.IntegerField(default=0, verbose_name='Курс')
    alias = models.CharField(max_length=100, verbose_name='Назва предмета')

    def __unicode__(self):
        return self.alias
   
    class Meta:
        verbose_name = "предмет"
        verbose_name_plural = "Предмети"



class LabWork(models.Model):
    '''Lab_works model'''
    subject_id = models.ForeignKey(Subject, verbose_name='Предмет')
    alias = models.CharField(max_length=100, verbose_name='Назва роботи')
    last_date = models.DateField()

    def __unicode__(self):
        return self.alias

    class Meta:
        verbose_name = "робота"
        verbose_name_plural = "Роботи"


def upload(instance, filename):
    username = instance.username.user.username
    print >> sys.stderr, username
    path = username + '\\' + str(instance.labwork_id.id) + '\\' + filename #
    print >> sys.stderr, path
    return path

class Files(models.Model):
    '''files store information'''
    username = models.ForeignKey(UserProfile)
    labwork_id = models.ForeignKey(LabWork)
    file_ext = models.CharField(max_length=5)
    file_n = models.FileField(upload_to=upload)
    upload_date = models.DateField()

    def filename(self):
        return os.path.basename(self.file_n.name)

    def __unicode__(self):
        return self.file_n.name

class FileForms(forms.ModelForm):
    class Meta:
        model = Files
        fields = ('file_n',)

    def __init__(self, *args, **kwargs):
        super(FileForms, self).__init__(*args, **kwargs)
        self.fields['file_n'].widget.attrs.update({'style' : 'display:none'})

class Comments(models.Model):
    '''Comments for files'''
    file_id = models.ForeignKey(Files)
    user = models.ForeignKey(UserProfile)
    post_date = models.TimeField()
    comment = models.CharField(max_length=500)
	
