from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from user_management.models import UserProfile
from file_storage.models import Files, Subject, LabWork, FileForms, Comments  # Create your views here.
from django import forms
from django.http import HttpResponse, Http404, HttpResponseRedirect
import sys
import datetime




def subjects(request):
    if request.method == 'GET':
        if request.user.is_staff:
            return staff_subj(request)
        else:
            usr = UserProfile.objects.get(user=request.user)
            subjs = Subject.objects.filter(course=usr.course)
            labs = {}
            for subj in subjs:
                labs[subj.id] = LabWork.objects.filter(subject_id=subj)
            return TemplateResponse(request, 'lab_list.html', {'labs': labs, 'subjects': subjs, 'user':request.user})


def lab_view(request, username, lab_id):
    if request.method == "GET":
        user=User.objects.get(username=username)
        usr = UserProfile.objects.get(user=user)
        try:
            lab = LabWork.objects.get(id=int(lab_id))
        except LabWork.DoesNotExist:
            raise Http404
        files = Files.objects.filter(username=usr, labwork_id=lab)
        frm = FileForms()
        comments = {}
        for ff in files:
            comments[ff.id] = Comments.objects.filter(file_id=ff)
        context = {'files': files, 'lab': lab, 'form': frm, 'user': request.user, 'comments': comments, 'path':request.path}
        return TemplateResponse(request, 'lab.html', context)
    elif request.method == "POST":
        usr = UserProfile.objects.get(user=request.user)
        form = FileForms(request.POST, request.FILES)
        if form.is_valid():
            lab = LabWork.objects.get(id=int(lab_id))
            upl_file = Files(username=usr, labwork_id=lab)
            upl_file.file_n = request.FILES['file_n']
            upl_file.upload_date = datetime.datetime.now()
            upl_file.save()
            return redirect(request.get_full_path())
        else:
            return HttpResponse('fail')
            
def post_comment(request):
    if request.method == 'GET':
        return HttpResponseRedirect('/')
    elif request.method == 'POST':
        data = request.POST
        fid=data.get('file_id')
        file_ins = Files.objects.get(id=int(fid))
        usrprof=UserProfile.objects.get(user=request.user)
        comment = Comments(file_id=file_ins, user=usrprof)
        comment.comment = data.get('comment')
        comment.post_date = datetime.datetime.now()
        comment.save()
        return HttpResponseRedirect(data.get('path'))


def staff_subj(request):
    subjects = Subject.objects.all().order_by('course')
    labs = LabWork.objects.all().order_by('subject_id')
    courses = []
    for subj in subjects:
        courses.append(subj.course)
    course_dict = {}
    for course in sorted(set(courses)):
        course_dict[course]= Subject.objects.filter(course=course)
    context = {'courses': course_dict, 'labs':labs}
    return TemplateResponse(request, 'staff_subj.html', context)
        

def staff_lab(request, lab_id):
    if request.user.is_staff:
        course = LabWork.objects.get(id=int(lab_id)).subject_id.course
        Students = UserProfile.objects.filter(course=course)
        groups = sorted(set(student.group_num for student in Students))
        print 'groups: ',groups
        context = {'lab_id': int(lab_id), "groups": groups, "students": Students}
        return TemplateResponse(request, 'staff_lab.html', context)
    else:
        return HttpResponseRedirect('/')





