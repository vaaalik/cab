# -*- coding: UTF-8 -*-
from django.contrib import admin
from file_storage.models import Subject, LabWork, Files, Comments
from django.contrib.sites.models import Site



class SubjectAdmin(admin.ModelAdmin):
    list_display = ('alias', 'course')


class LabWorkAdmin(admin.ModelAdmin):
    list_display = ('subject_id', 'alias', 'last_date')



#admin.site.unregister(Subject)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(LabWork, LabWorkAdmin)
#admin.site.register(Files)
admin.site.unregister(Site)