
from django.contrib import admin
from user_management.models import UserProfile

admin.autodiscover()

admin.site.register(UserProfile)