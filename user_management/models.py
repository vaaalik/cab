from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserProfile(models.Model):
 

    user = models.OneToOneField(User)
    group_num = models.CharField(max_length=3)
    course = models.IntegerField(default=0)
    username = models.CharField(_("User Name"), editable=False, max_length=30)

    def __unicode__(self):
        return self.user.username
